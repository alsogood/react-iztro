import { Star } from "iztro/lib/data/types";
import FunctionalStar from "iztro/lib/star/FunctionalStar";

export type IzstarProps = FunctionalStar | Star;
